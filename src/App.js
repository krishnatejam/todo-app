import React, { Component } from 'react';
import {Route,Switch} from 'react-router-dom';
import Login from './Login/Login';
import Dashbaord from './Dashboard/Dashboard';

class App extends Component {
  render() {
    return (
       <div>
           <Switch>
                <Route exact path='/' component={Login} />
                <Route exact path='/login' component={Login} />
                <Route exact path='/dashboard' component={Dashbaord} />
            </Switch>
       </div>
    );
  }
}

export default App;
