import React,{ Component } from 'react';
import './Dashboard.css';

class Dashbaord extends Component{

    constructor(props){
        super(props)
        this.state = {
            todos : [
                {data:'First....'},
                {data:'Second....'}
            ]
        }
    }


    updateTodos = () => {
        if(this.inputTodo.value){
            this.setState(
                {todos:[...this.state.todos,{data:this.inputTodo.value}]}
            )
        }
        this.inputTodo.value = null;
    }

    remove = (i) => {
         
         this.state.todos.splice(i,1);
         this.setState({
             todos:this.state.todos
         })
    }

    logout = () => {
       this.props.history.push('/');
    }

    render(){

        var todoLists = this.state.todos.map(function(item,i){;
            return(
               <div className="style-todo-block">
                  <div className="style-delete-icon" onClick={() => this.remove(i)}>
                     <i className="fa fa-trash" aria-hidden="true"></i>
                  </div>
                  <div className="style-todo">
                    {item.data}
                  </div>
               </div>
            );
        },this)

        return(
              <div>
                  <div className="todo-adder">
                     <input className="todo-input" type="text" placeholder="Enter Here..." ref={el => this.inputTodo = el} onChange={this.updateInput}/>
                     <button className="todo-add-button" onClick={this.updateTodos}>Add</button>
                     <button className="Log-out-button" onClick={this.logout}>Logout</button>
                  </div>
                  <div>{todoLists}</div>
              </div>
        );
    }
}

export default Dashbaord;